package org.idea.sandbox;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by rickwang on 2016/9/25.
 */

public class TinyChart extends View {
    static Logger LOG = LoggerFactory.getLogger(TinyChart.class);

    Rect size;

    Bitmap bitmap;
    Canvas canvas;

    Paint paint = new Paint();

    float horizontalSidePadding = 4f;
    float verticalSidePadding = 8f;
    float barPadding = 2f;
    float barWidth = 8f;
    float fillRatio = 0.9f;

    int maxCount = 8;
//    LinkedList<Float> values = new LinkedList<>(Arrays.asList(19.5f, 48.2f, 26.5f, 32.5f, 23.4f, 20.1f, 19.6f));
    LinkedList<Float> values = new LinkedList<>();

    public TinyChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TinyChart(Context context) {
        super(context);
    }

    // ======

    public void setColor(int color) {
        paint.setColor(color);
    }

    public void setHorizontalSidePadding(float horizontalSidePadding) {
        this.horizontalSidePadding = horizontalSidePadding;

        reconfigure();
    }

    public void setVerticalSidePadding(float verticalSidePadding) {
        this.verticalSidePadding = verticalSidePadding;
    }

    public void setBarPadding(float barPadding) {
        this.barPadding = barPadding;

        reconfigure();
    }

    public void setBarWidth(float barWidth) {
        this.barWidth = barWidth;

        reconfigure();
    }

    public void setFillRatio(float fillRatio) {
        this.fillRatio = fillRatio;
    }

    public int getMaxCount() {
        return maxCount;
    }

    // ======

    public void clear() {
        values.clear();
    }

    public void add(float value) {
        values.add(value);

        sweap();

        invalidate();
    }

    // ======

    protected void reconfigure() {
        maxCount = (int) ((size.width() - horizontalSidePadding - horizontalSidePadding - barPadding) / (barWidth + barPadding));

        sweap();
    }

    protected void sweap() {
        while (values.size() > maxCount) {
            values.removeFirst();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        size = new Rect(0, 0, w, h);

        reconfigure();

        bitmap = Bitmap.createBitmap(size.width(), size.height(), Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
    }

    @Override
    protected void onDraw(Canvas c) {
        drawBackground(canvas);

//        drawAxis(c);
        drawValues(canvas);

        c.drawBitmap(bitmap, 0f, 0f, paint);
    }

    protected void drawBackground(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    }

    protected void drawAxis(Canvas canvas) {
        int h = size.height();
        int w = size.width();

        float x0 = horizontalSidePadding;
        float y0 = h - verticalSidePadding;

        float x1 = w - horizontalSidePadding;
        float y1 = y0;

        canvas.drawLine(x0, y0, x1, y1, paint);
    }

    protected void drawValues(Canvas canvas) {
        if (values.isEmpty()) {
            return;
        }

        int h = size.height();

        float max = Collections.max(values);
        float ratio = fillRatio * (h - verticalSidePadding - verticalSidePadding) / max;

        float bottom = h - verticalSidePadding;

        float leftPadding = horizontalSidePadding + barPadding;

        int i = 0;
        for (float value : values) {
            float left = leftPadding + ((barPadding + barWidth) * i);
            float right = left + barWidth;
            float top = bottom - (value * ratio);

            canvas.drawRect(left, top, right, bottom, paint);

            i++;
        }
    }
}

package org.idea.sandbox;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    Handler handler;

    TinyChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler();

        chart = (TinyChart) findViewById(R.id.chart);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                chart.add((float)(Math.random() * 10));

                handler.postDelayed(this, 1000L);
            }
        }, 1000L);
    }
}
